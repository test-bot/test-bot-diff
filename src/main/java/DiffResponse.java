import java.util.List;

public class DiffResponse {
	private String reportId;
	private List<Change> changes;

	/**
	 * @return the reportId
	 */
	public String getReportId() {
		return reportId;
	}

	/**
	 * @param reportId the reportId to set
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	/**
	 * @return the changes
	 */
	public List<Change> getChanges() {
		return changes;
	}

	/**
	 * @param changes the changes to set
	 */
	public void setChanges(List<Change> changes) {
		this.changes = changes;
	}
}
