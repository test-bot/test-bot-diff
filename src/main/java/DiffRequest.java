
public class DiffRequest {
	private String oldSourceId;
	private String newSourceId;
	private String bucketName;
	private String region;

	/**
	 * @return the oldSource
	 */
	public String getOldSourceId() {
		return oldSourceId;
	}
	/**
	 * @param oldSourceId the oldSource to set
	 */
	public void setOldSourceId(String oldSourceId) {
		this.oldSourceId = oldSourceId;
	}
	/**
	 * @return the newSource
	 */
	public String getNewSourceId() {
		return newSourceId;
	}
	/**
	 * @param newSourceId the newSource to set
	 */
	public void setNewSourceId(String newSourceId) {
		this.newSourceId = newSourceId;
	}
	/**
	 * @return the bucketName
	 */
	public String getBucketName() {
		return bucketName;
	}
	/**
	 * @param bucketName the bucketName to set
	 */
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}
	/**
	 * @param region the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}
}
