import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.outerj.daisy.diff.HtmlCleaner;
import org.outerj.daisy.diff.html.HTMLDiffer;
import org.outerj.daisy.diff.html.HtmlSaxDiffOutput;
import org.outerj.daisy.diff.html.TextNodeComparator;
import org.outerj.daisy.diff.html.dom.DomTreeBuilder;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

public class LambdaHandler implements RequestHandler<DiffRequest, DiffResponse> {

    public DiffResponse handleRequest(DiffRequest input, Context context) {
    	AmazonS3 s3Client = new AmazonS3Client();
    	s3Client.setRegion(Region.getRegion(Regions.fromName(input.getRegion())));

        try {
        	context.getLogger().log("Old source: " + input.getOldSourceId());
        	context.getLogger().log("New source: " + input.getNewSourceId());

        	context.getLogger().log("Old source object retrival started from s3." + (new Date()).getTime());
            S3Object oldObject = s3Client.getObject(new GetObjectRequest(
            		input.getBucketName(), input.getOldSourceId()));
            context.getLogger().log("Old source object retrieved from s3." + (new Date()).getTime());

            context.getLogger().log("new source object retrieval started from s3." + (new Date()).getTime());
            S3Object newObject = s3Client.getObject(new GetObjectRequest(
            		input.getBucketName(), input.getNewSourceId()));
            context.getLogger().log("new source object retrieved from s3." + (new Date()).getTime());

            InputSource oldSource = new InputSource(oldObject.getObjectContent());
            InputSource newSource = new InputSource(newObject.getObjectContent());

            Locale locale = Locale.getDefault();

            context.getLogger().log("Cleaning started." + (new Date()).getTime());
            HtmlCleaner cleaner = new HtmlCleaner();

            DomTreeBuilder oldHandler = new DomTreeBuilder();
            cleaner.cleanAndParse(oldSource, oldHandler);

            TextNodeComparator leftComparator = new TextNodeComparator(
                    oldHandler, locale);

            DomTreeBuilder newHandler = new DomTreeBuilder();
            cleaner.cleanAndParse(newSource, newHandler);

            TextNodeComparator rightComparator = new TextNodeComparator(
                    newHandler, locale);
            context.getLogger().log("Cleaned and parsed both sources." + (new Date()).getTime());

            SAXTransformerFactory tf = (SAXTransformerFactory) TransformerFactory
                    .newInstance();

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            StreamResult streamResult = new StreamResult(outputStream);

            TransformerHandler result = tf.newTransformerHandler();
            result.setResult(streamResult);

            ContentHandlerDecorator decorator = new ContentHandlerDecorator(result);

            decorator.startDocument();
            decorator.startElement("", "diffreport", "diffreport", new AttributesImpl());
            decorator.startElement("", "diff", "diff", new AttributesImpl());

            context.getLogger().log("Diff started" + (new Date()).getTime());
            HtmlSaxDiffOutput output = new HtmlSaxDiffOutput(decorator,
                    "diff");

            HTMLDiffer differ = new HTMLDiffer(output);
            differ.diff(leftComparator, rightComparator);
            context.getLogger().log("Diff generated." + (new Date()).getTime());

            decorator.endElement("", "diff", "diff");
            decorator.endElement("", "diffreport", "diffreport");
            decorator.endDocument();

            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());

            String reportId = UUID.randomUUID().toString();
            s3Client.putObject(new PutObjectRequest(
	                 input.getBucketName(), reportId, inputStream, new ObjectMetadata()));
            context.getLogger().log("Report uploaded to s3." + (new Date()).getTime());

            inputStream.close();
            outputStream.close();

            DiffResponse response = new DiffResponse();
            response.setReportId(reportId);
            response.setChanges(decorator.getChanges());

            return response;

        } catch (AmazonServiceException ase) {
        	StringBuilder sb = new StringBuilder(
        			"Caught an AmazonServiceException, which" +
            		" means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.").append(System.lineSeparator());
            sb.append("Error Message:    " + ase.getMessage()).append(System.lineSeparator());
			sb.append("HTTP Status Code: " + ase.getStatusCode()).append(System.lineSeparator());
            sb.append("AWS Error Code:   " + ase.getErrorCode()).append(System.lineSeparator());
            sb.append("Error Type:       " + ase.getErrorType()).append(System.lineSeparator());
            sb.append("Request ID:       " + ase.getRequestId()).append(System.lineSeparator());

            context.getLogger().log(sb.toString());

            throw ase;
        } catch (AmazonClientException ace) {
        	StringBuilder sb = new StringBuilder(
        			"Caught an AmazonClientException, which means"+
            		" the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.").append(System.lineSeparator());
            sb.append("Error Message: " + ace.getMessage());

            context.getLogger().log(sb.toString());

            throw ace;
        } catch (IOException e) {
        	StringBuilder sb = new StringBuilder(
        			"Caught an IOException").append(System.lineSeparator());
            sb.append("Error Message: " + e.getMessage());

        	context.getLogger().log(sb.toString());

        	throw new RuntimeException("Caught an IOException", e);
		} catch (TransformerConfigurationException e) {
			StringBuilder sb = new StringBuilder(
        			"Caught a TransformerConfigurationException").append(System.lineSeparator());
            sb.append("Error Message: " + e.getMessage());

        	context.getLogger().log(sb.toString());

        	throw new RuntimeException("Caught a TransformerConfigurationException", e);
		} catch (SAXException e) {
			StringBuilder sb = new StringBuilder(
        			"Caught a SAXException").append(System.lineSeparator());
            sb.append("Error Message: " + e.getMessage());

        	context.getLogger().log(sb.toString());

        	throw new RuntimeException("Caught a SAXException", e);
		}
    }

}
