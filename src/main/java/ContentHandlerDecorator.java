import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.TransformerHandler;

import org.owasp.html.Sanitizers;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;


public class ContentHandlerDecorator implements TransformerHandler {
	private TransformerHandler handler;
	private StringBuilder builder;
	private boolean isRecording;
	private int depth = 0;
	private String currentChangeId;
	private String currentChangeType;
	private ArrayList<Change> changes = new ArrayList<Change>();

	public ContentHandlerDecorator(TransformerHandler handler) {
		this.handler = handler;
	}

	public ContentHandlerDecorator(ContentHandler handler) {
		this.handler = (TransformerHandler)handler;
	}

	@Override
	public void setDocumentLocator(Locator locator) {
		this.handler.setDocumentLocator(locator);
	}

	@Override
	public void startDocument() throws SAXException {
		this.handler.startDocument();
	}

	@Override
	public void endDocument() throws SAXException {
		this.handler.endDocument();
	}

	@Override
	public void startPrefixMapping(String prefix, String uri) throws SAXException {
		this.handler.startPrefixMapping(prefix, uri);
	}

	@Override
	public void endPrefixMapping(String prefix) throws SAXException {
		this.handler.endPrefixMapping(prefix);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		if(this.isRecording) {
			this.recordContent(uri, localName, qName, atts);
		} else {
			String changeId = atts.getValue("changeId");
			String id = atts.getValue("id");
			if(changeId != null) {
	
				String elemClass = atts.getValue("class");
				switch (elemClass) {
				case "diff-html-added":
					this.startRecordingElementContent("added", changeId);
					break;
				case "diff-html-changed":
					Change change = new Change();
					change.setType("changed");
					change.setId(changeId);
					change.setChanges(atts.getValue("changes"));
					this.changes.add(change);
					break;
				case "diff-html-removed":
					this.startRecordingElementContent("removed", changeId);
					break;
				default:
					break;
				}
			}
		}

		this.handler.startElement(uri, localName, qName, atts);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(this.isRecording) {
			this.processEndElement();
		}
		this.handler.endElement(uri, localName, qName);
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if(this.isRecording) {
			this.builder.append(new String(ch, start, length));
		}
		this.handler.characters(ch, start, length);
	}

	@Override
	public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
		this.handler.ignorableWhitespace(ch, start, length);
	}

	@Override
	public void processingInstruction(String target, String data) throws SAXException {
		this.handler.processingInstruction(target, data);
	}

	@Override
	public void skippedEntity(String name) throws SAXException {
		this.handler.skippedEntity(name);
	}

	@Override
	public void startDTD(String name, String publicId, String systemId) throws SAXException {
		this.handler.startDTD(name, publicId, systemId);
	}

	@Override
	public void endDTD() throws SAXException {
		this.handler.endDTD();
	}

	@Override
	public void startEntity(String name) throws SAXException {
		this.handler.startEntity(name);
	}

	@Override
	public void endEntity(String name) throws SAXException {
		this.handler.endEntity(name);
	}

	@Override
	public void startCDATA() throws SAXException {
		this.handler.startCDATA();
	}

	@Override
	public void endCDATA() throws SAXException {
		this.handler.endCDATA();
	}

	@Override
	public void comment(char[] ch, int start, int length) throws SAXException {
		this.handler.comment(ch, start, length);
	}

	@Override
	public void notationDecl(String name, String publicId, String systemId) throws SAXException {
		this.handler.notationDecl(name, publicId, systemId);
	}

	@Override
	public void unparsedEntityDecl(String name, String publicId, String systemId, String notationName)
			throws SAXException {
		this.handler.unparsedEntityDecl(name, publicId, systemId, notationName);
	}

	@Override
	public void setResult(Result result) throws IllegalArgumentException {
		this.handler.setResult(result);
	}

	@Override
	public void setSystemId(String systemID) {
		this.handler.setSystemId(systemID);
	}

	@Override
	public String getSystemId() {
		return this.handler.getSystemId();
	}

	@Override
	public Transformer getTransformer() {
		return this.handler.getTransformer();
	}

	public List<Change> getChanges() {
		return this.changes;
	}

	private void startRecordingElementContent(String changeType, String changeId) {
		this.isRecording = true;
		this.depth++;
		this.builder = new StringBuilder();
		this.currentChangeId = changeId;
		this.currentChangeType = changeType;
	}

	private void recordContent(String uri, String localName, String qName, Attributes atts) {
		this.builder.append("<" + qName);
		
		if(atts.getLength() > 0) {
			this.builder.append(" ");
			for (int i = 0; i < atts.getLength(); i++) {
				String attrName = atts.getQName(i);
				String attrValue = atts.getValue(i);
				this.builder.append(attrName).append("=").append("\"" + attrValue + "\"").append(" ");
			}
		}

		this.builder.append(">");
	}

	private void processEndElement() {
		this.depth--;
		if(this.depth == 0) {
			this.isRecording = false;

			Change change = new Change();
			change.setId(this.currentChangeId);
			change.setType(this.currentChangeType);
			change.setChanges(this.builder.toString());
			this.changes.add(change);

			this.currentChangeId = null;
			this.currentChangeType = null;
			this.builder = null;
		}
		
	}
}
