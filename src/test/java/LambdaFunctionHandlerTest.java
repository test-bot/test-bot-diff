
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
public class LambdaFunctionHandlerTest {

    private static DiffRequest input;
    private static String bucketName = "test-bot-diff-integration-tests";
    private static String oldKey = "testOld.html";
    private static String newKey = "testNew.html";
    private static AmazonS3 s3Client = new AmazonS3Client();

    @BeforeClass
    public static void initSdl() {
    	s3Client.setRegion(Region.getRegion(Regions.EU_CENTRAL_1));
    }

    @BeforeClass
    public static void createInput() throws IOException {
        input = new DiffRequest();
        input.setBucketName(bucketName);
        input.setOldSourceId(oldKey);
        input.setNewSourceId(newKey);
        input.setRegion("eu-central-1");
    }

    @BeforeClass
    public static void ensureTestFilesInBucket() {
    	ObjectListing objects = s3Client.listObjects(new ListObjectsRequest()
    			.withBucketName(bucketName).withPrefix(oldKey));

    	boolean oldFound = false;
        for (S3ObjectSummary objectSummary: objects.getObjectSummaries()) {
            if (objectSummary.getKey().equals(oldKey)) {
                oldFound = true;
                break;
            }
        }

    	ObjectListing objects2 = s3Client.listObjects(new ListObjectsRequest()
    			.withBucketName(bucketName).withPrefix(newKey));

    	boolean newFound = false;
        for (S3ObjectSummary objectSummary: objects2.getObjectSummaries()) {
            if (objectSummary.getKey().equals(newKey)) {
                newFound = true;
                break;
            }
        }
 
        if(!oldFound){
        	s3Client.putObject(bucketName, oldKey, new File("src/test/resources/testOld.html"));
        }

        if(!newFound){
        	s3Client.putObject(bucketName, newKey, new File("src/test/resources/testNew.html"));
        }
    }

    private Context createContext() {
        TestContext ctx = new TestContext();

        // TODO: customize your context here if needed.
        ctx.setFunctionName("Your Function Name");

        return ctx;
    }

    @Test
    public void shouldReturnChanges() {
        LambdaHandler handler = new LambdaHandler();
        Context ctx = createContext();

        DiffResponse output = handler.handleRequest(input, ctx);

        if (output != null) {
            List<Change> changes = output.getChanges();
            assertEquals(21, changes.size());
        }

        s3Client.deleteObject(bucketName, output.getReportId());
    }

    @Test
    public void shouldUploadreportToS3() throws IOException {
        LambdaHandler handler = new LambdaHandler();
        Context ctx = createContext();

        DiffResponse output = handler.handleRequest(input, ctx);

        // Will throw Not Found exception.
        S3Object obj = s3Client.getObject(bucketName, output.getReportId());
        obj.close();

        s3Client.deleteObject(bucketName, output.getReportId());
    }
}
